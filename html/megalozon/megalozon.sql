-- psql -U postgres -f megalozon.sql
-- psql "host='localhost' port=5432 dbname=megalozon user=megaluser password='megaluser'"

DROP DATABASE IF EXISTS megalozon;
CREATE DATABASE megalozon;
\connect megalozon

SELECT current_database();
SELECT current_user;

CREATE TABLE category (
  id INTEGER PRIMARY KEY, 
  name TEXT,
  image TEXT 
);

INSERT INTO category VALUES(0, 'yachts', 'img/yacht.jpg');
INSERT INTO category VALUES(1, 'supercars', 'img/supercar.jpg');
INSERT INTO category VALUES(2, 'majordomes', 'img/majordome.jpg');

CREATE TABLE product (
  id INTEGER PRIMARY KEY, 
  category INTEGER, 
  name TEXT, 
  price INTEGER, 
  image TEXT, 
  FOREIGN KEY(category) REFERENCES category(id)
);

INSERT INTO product VALUES(0, 0, 'sebago', 50000000, 'img/sebago.jpg');
INSERT INTO product VALUES(1, 0, 'rvyacht', 80000000, 'img/rvyacht.jpg');
INSERT INTO product VALUES(2, 0, 'ssfg', 200000000, 'img/ssfg.jpg');
INSERT INTO product VALUES(3, 1, 'couletach', 2000000, 'img/couletach.jpg');
INSERT INTO product VALUES(4, 1, 'supercrade', 1000000, 'img/supercrade.jpg');
INSERT INTO product VALUES(5, 2, 'alfred', 300000000, 'img/alfred.jpg');
INSERT INTO product VALUES(6, 2, 'nestor', 100000000, 'img/nestor.jpg');

DROP ROLE IF EXISTS megaluser;
CREATE ROLE megaluser WITH LOGIN PASSWORD 'megaluser';
GRANT SELECT ON TABLE category TO megaluser;
GRANT SELECT ON TABLE product TO megaluser;

