{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)

data Category = Category
    {
        _idCategory :: Int,
        _nameCategory :: T.Text, 
        _imgCategory :: T.Text
    } deriving (Show)

data Product = Product
    {
        _idProduct :: Int,
        _categoryProduct :: Int,
        _nameProduct :: T.Text,
        _price :: Int,
        _imgProduct :: T.Text
    } deriving (Show)
    
instance FromRow Category where
    fromRow = Category <$> field <*> field <*> field

instance FromRow Product where
    fromRow = Product <$> field <*> field <*> field <*> field <*> field

getCategories :: IO ()
getCategories  = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=megalozon user=megaluser password='megaluser'"
    res <- SQL.query_ conn "SELECT * FROM category" :: IO [Category]
    mapM_ print res

getProducts :: IO ()
getProducts = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=megalozon user=megaluser password='megaluser'"
    res <- SQL.query_ conn "SELECT * FROM product" :: IO [Product]
    mapM_ print res

main :: IO ()
main = do
    conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=megalozon user=megaluser password='megaluser'"
    getCategories
    getProducts
    SQL.close conn
        
    
