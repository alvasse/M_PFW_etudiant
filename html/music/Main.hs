{-# LANGUAGE OverloadedStrings #-}

import qualified Data.Text as T
import qualified Database.SQLite.Simple as SQL
import Database.SQLite.Simple.FromRow (FromRow, fromRow, field)

data Name = Name T.Text deriving Show

instance FromRow Name where
    fromRow = Name <$> field 

main :: IO ()
main = do
    conn <- SQL.open "mybase.db"

    putStrLn "\n*** id et nom des artistes ***"
    res <- SQL.query_ conn "SELECT * FROM artists" :: IO [(Int,T.Text)]
    mapM_ print res

    putStrLn "\n*** id et nom de l'artiste 'Radiohead' ***"
    
    res2 <- SQL.query conn "SELECT * FROM artists WHERE lower(name) LIKE ?" (SQL.Only ("%Radiohead%"::T.Text)) :: IO [(Int, T.Text)]
    mapM_ print res2

    putStrLn "\n*** tous les champs des titres dont le nom contient 'ust' et dont l'id > 1 ***"
    res3 <- SQL.query conn "SELECT * FROM titles WHERE id > ? AND lower(name) LIKE ?" (1::Int, "%ust%"::T.Text) :: IO [(Int, Int, T.Text)]
    mapM_ print res3

    putStrLn "\n*** noms des titres (en utilisant le type Name) ***"
    res4 <- SQL.query_ conn "SELECT name FROM titles" :: IO [Name]
    mapM_ print res4

    putStrLn "\n*** noms des titres (en utilisant une liste de T.Text) ***"
    res5 <- SQL.query_ conn "SELECT name FROM titles" :: IO [[T.Text]]
    mapM_ print res5

    SQL.close conn

