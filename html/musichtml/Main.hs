{-# LANGUAGE OverloadedStrings #-}
import qualified Clay as C
import qualified Data.Text as T
import qualified Database.PostgreSQL.Simple as SQL
import qualified Data.Text.IO as TIO
import qualified Data.Text.Lazy as L
import           Lucid
import Database.PostgreSQL.Simple.FromRow (FromRow, fromRow, field)
import           System.Environment (getArgs, getProgName)

data Music = Music
    {
        _artist :: T.Text, 
        _title :: T.Text
    } deriving (Show)
    
instance FromRow Music where
    fromRow = Music <$> field <*> field

myPage :: [Music] -> Html()
myPage musics = do
    doctype_
    html_ $ do
        head_ $ meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "my page"
            ul_ $ mapM_ (li_ . toHtml . joinMusic) musics

joinMusic :: Music -> T.Text
joinMusic m = T.concat [_title m, " - ", _artist m]

main :: IO ()
main = do
    args <- getArgs
    if length args /= 1
    then do
        progName <- getProgName
        putStrLn $ "usage: " ++ progName ++ " <output html>"
    else do
        let [output] = args
        conn <- SQL.connectPostgreSQL "host='localhost' port=5432 dbname=musicpg user=toto password='toto'"   
        
        putStrLn "\n*** Titre et nom des artistes ***"
        res <- SQL.query_ conn "SELECT a.name, t.name FROM artists a LEFT JOIN titles t ON a.id = t.artist" :: IO [Music]
            
        renderToFile output $ myPage res -- sortie dans un fichier
        SQL.close conn
    
