{-# LANGUAGE MultiParamTypeClasses      #-}
{-# LANGUAGE OverloadedStrings          #-}
{-# LANGUAGE QuasiQuotes                #-}
{-# LANGUAGE TemplateHaskell            #-}
{-# LANGUAGE TypeFamilies               #-}

import qualified Data.Text as T
import           Data.Text.Encoding (decodeUtf8)
import           Graphics.Plotly
import           Graphics.Plotly.Lucid
import           Lens.Micro
import           Lucid
import           Yesod
import           Yesod.Lucid

import qualified SpuriousCorrelation as SC
import qualified Meteorites as M

meteoritesCsv = "data/meteorites.csv"
port = 3000

data App = App

instance Yesod App

instance RenderMessage App FormMessage where
  renderMessage _ _ = defaultFormMessage

mkYesod "App" [parseRoutes|
  /                       HomeR                   GET
  /spuriousCorrelation    SpuriousCorrelationR    GET
  /meteorites             MeteoritesR             GET
  |]

-- home

getHomeR = defaultLayout $ do
  [whamlet|
    <h1> Some plots
    <p> <a href=@{SpuriousCorrelationR}> Spurious correlation
    <p> <a href=@{MeteoritesR}> Meteorites
    |]

-- spurious correlation

scaledData = map scale SC.allData
  where scale (SC.Data y p l) = SC.Data y (div p 12) l

spuriousCorrelationPlot = plotly "myDiv" 
  [ line (aes & x .~ SC.year & y .~ SC.sociologyPhd) scaledData & mode ?~ [Lines, Markers] & name ?~ "Doctorates (/12)",
    line (aes & x .~ SC.year & y .~ SC.spaceLaunches) scaledData & mode ?~ [Lines, Markers] & name ?~ "Space launches" ]
  & layout . width ?~ 800
  & layout . height ?~ 400
  & layout . margin ?~ thinMargins
  & layout . showlegend ?~ True

-- TODO spurious correlation (html)
getSpuriousCorrelationR = lucid $ \url -> do
  html_ $ do
    body_ $ do
      p_ "TODO"

-- meteorites

mkTrace meteorites classname = 
  points (aes & x .~ M.longitude 
              & y .~ M.latitude) 
    (filter (((==) classname) . M.classname) meteorites) 
  & name ?~ decodeUtf8 classname

meteoritesPlot traces = plotly "myPlotDiv"
  traces
  & layout . width ?~ 800
  & layout . height ?~ 400
  & layout . margin ?~ thinMargins
  & layout . showlegend ?~ True

-- TODO filter meteorites inside USA
-- latitude in [15, 50], longitude in [-130, -70]
getMeteoritesR = do
  meteoritesWorld <- liftIO $ M.readCsv meteoritesCsv
  let classnames = M.getClassnames meteoritesWorld
      traces = map (mkTrace meteoritesWorld) classnames
  lucid $ \url -> do
    html_ $ do
      head_ $ do
        title_ "Meteorites"
        plotlyCDN
      body_ $ do
        h1_ $ "Meteorites"
        p_ "Meteorite landing locations in the USA."
        p_ $ do
          "source: "
          a_ [href_ "https://data.nasa.gov/Space-Science/Meteorite-Landings/gh4g-9sfh"] "NASA"
        Lucid.toHtml $ meteoritesPlot traces
        p_ $ a_ [href_ "/"] "back home"

-- main

main :: IO ()
main = do
  meteoritesWorld <- liftIO $ M.readCsv meteoritesCsv
  print $ M.getClassnames meteoritesWorld
  warp port App

