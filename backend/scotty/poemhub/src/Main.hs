{-# LANGUAGE OverloadedStrings #-}

import Control.Monad.Trans (liftIO)
import Network.Wai.Middleware.RequestLogger (logStdoutDev)
import Web.Scotty (get, middleware, param, post, rescue, scotty, html)

import qualified Model
import qualified View

-- TODO implement routes

main = scotty 3000 $ do

  middleware logStdoutDev

  get "/" $ do
    poems <- liftIO Model.getPoems
    html $ View.mkpage "Poem hub - Home" $ View.homeRoute poems