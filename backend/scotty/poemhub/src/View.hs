{-# LANGUAGE OverloadedStrings #-}

module View (mkpage, homeRoute) where

import qualified Clay as C
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Lucid

import qualified Model

-- TODO implement CSS styles
-- TODO implement route views

mkpage :: Lucid.Html () -> Lucid.Html () -> L.Text
mkpage titleStr page = renderText $ html_ $ do
  head_ $ title_ titleStr
  body_ page

getPoemDiv :: Model.Poem -> Html()
getPoemDiv p = 
    div_ [class_ "divCss"] $ do 
      h3_ [class_ "h3Css"] (toHtml (Model.title p))
      p_ [class_ "pCss"] toHtml (T.concat ["By ", Model.author p, "(", Model.getYearText p, ")"])

homeRoute :: [Model.Poem] -> Lucid.Html ()
homeRoute poems = do 
  h1_ "Poem hub - Home"
  p_ "Write a new Poem"
  div_ $ mapM_ getPoemDiv poems