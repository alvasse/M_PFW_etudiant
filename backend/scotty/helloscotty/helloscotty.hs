{-# LANGUAGE OverloadedStrings #-}
import qualified Data.Text as T
import qualified Data.Text.Lazy as L
import           Web.Scotty (scotty, get, html, param, rescue)
import           Lucid

main = scotty 3000 $ do
    get "/" $ html $ renderText $ html_ $ do
        head_ $ meta_ [charset_ "utf-8"]
        body_ $ do
            h1_ "The Hello Scotty Project"
            a_ [href_ "/hello"] (p_ "Go to hello page")

    get "/hello" $ do
        name <- param "name" `rescue` (\_ -> return ("unknown user"::String))
        html $ renderText $ html_ $ do
            head_ $ meta_ [charset_ "utf-8"]
            body_ $ do
                h1_ "Hello route"
                p_ $ toHtml ("Hello " ++ name)
                form_ [action_ "/hello", method_ "get"] $ do
                    input_ [name_ "name", value_ "enter text here"]
                    input_ [type_ "submit"]