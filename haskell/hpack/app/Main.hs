import qualified Data.Text.IO as TIO
import qualified Data.Text as T
import           System.Environment (getArgs)

-- main :: IO ()
-- main =  getArgs 
--     >>= readFile . head 
--     >>= (mapM_ putStrLn) . lines

-- main = do
--     [filename] <- getArgs
--     file <- readFile filename 
--     mapM_ putStrLn (lines file)

-- main = do
--     [filename] <- getArgs
--     file <- TIO.readFile filename 
--     mapM_ TIO.putStrLn (T.lines file)
--     -- TIO.putStrLn file

process filename = do 
    file <- TIO.readFile filename 
    mapM_ TIO.putStrLn (T.lines file)
    -- TIO.putStrLn file

main = do
    args <- getArgs
    let usagemsg = "usage example: hpack.out filename"
    case args of 
        [filename] -> process filename
        _        -> error $ "args length does not equal 1. args: : " ++ show args ++ "\n" ++ usagemsg
    