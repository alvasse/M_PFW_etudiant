
## build

```
$ nix-build
$ firefox result/index.html
```

## shell

```
$ nix-shell
$ cabal new-build all
$ ln -s dist-newstyle/build/x86_64-linux/ghcjs-0.2.1/bmxriders-client-reflex-0.1.0.0/c/bmxriders-client-reflex/build/bmxriders-client-reflex/bmxriders-client-reflex.jsexe/all.js .
firefox index.html
```

