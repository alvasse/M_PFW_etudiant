{-# LANGUAGE DeriveGeneric #-}

module Repository where

import Data.Aeson
import Data.Text
import GHC.Generics
import User

data Repository = Repository 
    { 
        id :: Int, 
        name :: Text,
        fullName :: Text,
        owner :: User
    } deriving (Eq, Generic, Show)

instance ToJSON Repository
instance FromJSON Repository

