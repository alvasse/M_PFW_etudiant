{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Common
import User
import Repository

import Data.Proxy 
import Data.Text
import Network.HTTP.Client (newManager, defaultManagerSettings)
import Servant
import Servant.Client

getUser :: Text -> Maybe Text -> ClientM (Maybe User)
getRepository :: Int -> Maybe Text -> ClientM (Maybe Repository)
getUserRepositories :: Text -> Maybe Text -> ClientM [Repository]

getUser :<|> getRepository :<|> getUserRepositories = client (Proxy @MyhubApi)

agent :: Maybe Text
agent = Just "Servant Client for Myhub"

main :: IO ()
main = do
    mgr <- newManager defaultManagerSettings
    let env = ClientEnv mgr (BaseUrl Http "localhost" 3000 "")

    runClientM (getUser "juliendehos" agent) env >>= print
    runClientM (getRepository 1 agent) env >>= print
    runClientM (getUserRepositories "juliendehos" agent) env >>= print

