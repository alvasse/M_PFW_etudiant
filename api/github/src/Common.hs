{-# LANGUAGE DataKinds #-}
{-# LANGUAGE TypeOperators #-}

module Common where

import User 
import Repository

import Data.Text
import Servant

type AgentHeader = Header "User-Agent" Text

type UserApi = "users" :> Capture "user" Text :> AgentHeader :> Get '[JSON] (Maybe User)

type RepositoryApi = "repository" :> Capture "repository" Int :> AgentHeader :> Get '[JSON] (Maybe Repository)

type UserRepositoriesApi = "repositories" :> Capture "repositories" Text :> AgentHeader :> Get '[JSON] [Repository]

type MyhubApi = 
    UserApi 
    :<|> RepositoryApi
    :<|> UserRepositoriesApi

type GithubApi = 
    UserApi 
    :<|> RepositoryApi
    :<|> UserRepositoriesApi