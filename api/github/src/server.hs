{-# LANGUAGE DataKinds #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeApplications #-}
{-# LANGUAGE TypeOperators #-}

import Common
import User
import Repository

import Data.Maybe
import qualified Data.Text as T
import Network.Wai.Handler.Warp (run)
import Servant

myUsers :: [User]
myUsers = [ User 2 "spongebob", User 1 "johndoe", User 171292423 "juliendehos"]

findUser :: [User] -> T.Text -> Maybe User
findUser users userName = listToMaybe $ filter ((==) userName . login) users

handleUser :: T.Text -> h -> Handler (Maybe User)
handleUser userName _ = return (findUser myUsers userName)

myRepositories :: [Repository]
myRepositories = [ 
    Repository 1 "ELF" "juliendehos/ELF" (User 171292423 "juliendehos"),
    Repository 2 "MonSuperRepo" "alexandrevasse/MonSuperRepo" (User 1 "alexandrevasse"),
    Repository 3 "MonMoinsBonRepo" "alexandrevasse/MonMoinsBonRepo" (User 1 "alexandrevasse")]

findRepository :: [Repository] -> Int -> Maybe Repository
findRepository repositories repoId = listToMaybe $ filter ((==) repoId . Repository.id) repositories

handleRepository :: Int -> h -> Handler (Maybe Repository)
handleRepository repoId _ = return (findRepository myRepositories repoId)

-- findUserRepositories :: [Repository] -> T.Text -> [Repository]
-- findUserRepositories repositories userName = filter ((==) userName . Repository.owner.login) repositories

findUserRepositories :: [Repository] -> T.Text -> [Repository]
findUserRepositories repositories userName = repositories

handleUserRepositories :: T.Text -> h -> Handler [Repository]
handleUserRepositories userName _ = return (findUserRepositories myRepositories userName)

server :: Server MyhubApi
server = 
    handleUser
    :<|> handleRepository
    :<|> handleUserRepositories

main :: IO ()
main = run 3000 $ serve (Proxy @MyhubApi) server

