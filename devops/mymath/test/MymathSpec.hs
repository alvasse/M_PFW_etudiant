module MymathSpec where

import Test.Hspec

import Mymath

main :: IO ()
main = hspec spec

spec :: Spec
spec = describe "mymath" $ do
  it "square 2 == 4" $ square (2::Int) `shouldBe` 4
  it "square 3 == 9" $ square (3::Int) `shouldBe` 9
  it "square (-2) == 4" $ square (-2::Int) `shouldBe` 4


